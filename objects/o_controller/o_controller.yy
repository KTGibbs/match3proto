{
    "id": "babb519f-7745-47b8-9ad0-a33034bf3c4f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_controller",
    "eventList": [
        {
            "id": "d4a08b62-b45c-4740-838c-12e5c472cab6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "babb519f-7745-47b8-9ad0-a33034bf3c4f"
        },
        {
            "id": "c058b1ed-1f75-47eb-9308-6691a02f46fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "babb519f-7745-47b8-9ad0-a33034bf3c4f"
        },
        {
            "id": "74cb801d-9b4c-474c-9b6b-144e705b1843",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "babb519f-7745-47b8-9ad0-a33034bf3c4f"
        },
        {
            "id": "b75f356e-13a1-43f8-a7ea-ebb4c4609fe7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "babb519f-7745-47b8-9ad0-a33034bf3c4f"
        },
        {
            "id": "258e752b-4737-45b0-ad81-946339eb7bc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "babb519f-7745-47b8-9ad0-a33034bf3c4f"
        },
        {
            "id": "05488654-3f52-4999-b28d-6eed3003b3a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "babb519f-7745-47b8-9ad0-a33034bf3c4f"
        },
        {
            "id": "822892a6-1ca8-490b-b5f7-edb2dcdc0a27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "babb519f-7745-47b8-9ad0-a33034bf3c4f"
        },
        {
            "id": "70357fd5-39b8-4880-b6df-66c45c37844b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "babb519f-7745-47b8-9ad0-a33034bf3c4f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}