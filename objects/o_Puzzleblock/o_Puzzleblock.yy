{
    "id": "1d97f1cb-ecb1-4a35-8156-e965d9176305",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_Puzzleblock",
    "eventList": [
        {
            "id": "6b8c9cfd-44a1-4054-9dba-4d8c8a9cd0ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d97f1cb-ecb1-4a35-8156-e965d9176305"
        },
        {
            "id": "fa805b46-7a1b-4c99-bad7-38ceff03ca7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1d97f1cb-ecb1-4a35-8156-e965d9176305"
        },
        {
            "id": "a5dd3841-4b0f-4177-a826-325645fdfb46",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "1d97f1cb-ecb1-4a35-8156-e965d9176305"
        },
        {
            "id": "e24fa655-a400-45bb-8200-3ab274945b6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1d97f1cb-ecb1-4a35-8156-e965d9176305"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dde1c97f-1284-41c0-b560-bac02ea5f4c6",
    "visible": true
}