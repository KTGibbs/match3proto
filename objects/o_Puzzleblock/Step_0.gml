/// @desc Check if blocks exist/if they match

#region Is there a block on the right/left of me?
if(done_spawning){		//done_spawning is just a timer after the last line of blocks spawns (alarm[4]
	if(instance_place(x+32,y,o_Puzzleblock)){
		block_right = instance_place(x+32,y,o_Puzzleblock);
	}
	else{
		block_right = noone;
	}
	
	if(instance_place(x-32,y,o_Puzzleblock)){
		block_left = instance_place(x-32,y,o_Puzzleblock);
	}
	
	else{
		block_left = noone;
	}
	
	if(instance_place(x,y+32,o_Puzzleblock)){
		block_below = instance_place(x,y+32,o_Puzzleblock);
	}
	
	else{
		block_below = noone;
	}
	
	if(can_move){
		if(block_below = noone){
			if(y != global.PuzzleBottom){
				y += 32;
			}
		}
	}
	
#endregion

#region If it exists, does it match me?
	if(block_right != noone){
		
		if(block_right.image_index = image_index){
			match_right = true;
		}
	

		if(block_right.image_index != image_index){
			match_right = false;
		}
	}
	
	if(block_left != noone){
		
		if(block_left.image_index = image_index){
			match_left = true;
		}

	
		if(block_left.image_index != image_index){
			match_left = false;
		}
	}
#endregion

#region If right/left match me, TAKE US OUT (and spawn replacements)
	if(match_right && match_left){
		var newBlock1 = instance_create_depth(x, y-1024, -1, o_Puzzleblock);
		var newBlock2 = instance_create_depth(x-32, y-1024, -1, o_Puzzleblock);
		var newBlock3 = instance_create_depth(x+32, y-1024, -1, o_Puzzleblock);
		
		newBlock1.done_spawning = true;
		newBlock1.block_below = noone;
		
		newBlock2.done_spawning = true;
		newBlock2.block_below = noone;
		
		newBlock3.done_spawning = true;
		newBlock3.block_below = noone;
		
		instance_destroy(block_right);
		instance_destroy(block_left);
		instance_destroy();
	}
}
#endregion

