{
    "id": "e352d216-c3f9-4f72-afee-c9fa3223c865",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cursor",
    "eventList": [
        {
            "id": "6abf7051-de2b-4cff-8def-180fbc0b23d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e352d216-c3f9-4f72-afee-c9fa3223c865"
        },
        {
            "id": "54d0d592-672f-4532-9530-a1b34f684124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e352d216-c3f9-4f72-afee-c9fa3223c865"
        },
        {
            "id": "7b2a97c3-b0c3-4485-878d-2299f1966edb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e352d216-c3f9-4f72-afee-c9fa3223c865"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0cecb0f9-58e1-448e-a4fb-8f5e8bc19f87",
    "visible": true
}