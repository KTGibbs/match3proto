/// @desc
x = mouse_x;
y = mouse_y;

mouse_left = mouse_check_button_pressed(mb_left);
mouse_right = mouse_check_button_pressed(mb_right);

if(place_meeting(x,y,o_Puzzleblock)){
	if(mouse_left){
		block1 = instance_place(x,y,o_Puzzleblock);
		if(instance_exists(o_selectedBox)){
			instance_destroy(o_selectedBox);
		}
		instance_create_depth(block1.x,block1.y, -2, o_selectedBox);
	}
}

if(place_meeting(x,y,o_Puzzleblock) && block1 != noone){
	 if(mouse_right){
		block2 = instance_place(x,y,o_Puzzleblock);
	}
}

if(block1 != noone && block2 != noone){
	scr_swap(block1.x, block1.y, block2.x, block2.y);
	block1 = noone;
	block2 = noone;
}

