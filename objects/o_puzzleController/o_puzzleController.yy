{
    "id": "3a7f9f2c-72cd-4506-97ca-637fb2910c02",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_puzzleController",
    "eventList": [
        {
            "id": "689f204f-2cba-4dca-9f3f-8b56ed8e337d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a7f9f2c-72cd-4506-97ca-637fb2910c02"
        },
        {
            "id": "03806fdf-9150-4c42-98e5-86dc4aa84586",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3a7f9f2c-72cd-4506-97ca-637fb2910c02"
        },
        {
            "id": "edf75232-d7f1-4af7-b309-a09eae74fb61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3a7f9f2c-72cd-4506-97ca-637fb2910c02"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "af7822ee-0e6e-41a7-8a17-2578f5f0d494",
    "visible": true
}