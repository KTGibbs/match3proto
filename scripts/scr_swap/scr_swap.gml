var tempx, tempy;

argument0 = block1.x;
argument1 = block1.y;

tempx = block1.x;
tempy = block1.y;

argument2 = block2.x;
argument3 = block2.y;

o_controller.draw_thang = true;

block1.match_left = false;
block1.match_right = false;

block2.match_left = false;
block2.match_right = false;

block1.block_below = noone;
block2.block_below = noone;

block1.x = block2.x;
block1.y = block2.y;

block2.x = tempx;
block2.y = tempy;

if(instance_exists(o_selectedBox)){
	with(o_selectedBox){
			instance_destroy();
	}
}