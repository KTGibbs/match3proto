{
    "id": "dde1c97f-1284-41c0-b560-bac02ea5f4c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Puzzleblock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ecbccb3-e93e-422e-bcb8-6d3b3680391d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde1c97f-1284-41c0-b560-bac02ea5f4c6",
            "compositeImage": {
                "id": "34313a7a-67bb-447d-8e1d-7254f6a5c608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ecbccb3-e93e-422e-bcb8-6d3b3680391d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25270aa6-2144-4d7f-9205-41ba4f1e85ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ecbccb3-e93e-422e-bcb8-6d3b3680391d",
                    "LayerId": "7421c57d-3c03-4b92-9227-e6063ce55320"
                }
            ]
        },
        {
            "id": "685017f4-cbc1-46f9-9938-44861ecf70fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde1c97f-1284-41c0-b560-bac02ea5f4c6",
            "compositeImage": {
                "id": "282194ed-57d1-4fb4-ba69-0c01091f579c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "685017f4-cbc1-46f9-9938-44861ecf70fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb954d44-37dc-401a-9add-1b68ae21e427",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "685017f4-cbc1-46f9-9938-44861ecf70fd",
                    "LayerId": "7421c57d-3c03-4b92-9227-e6063ce55320"
                }
            ]
        },
        {
            "id": "62ca34e6-473d-427f-aaf3-15364eb9d79b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde1c97f-1284-41c0-b560-bac02ea5f4c6",
            "compositeImage": {
                "id": "e463ff32-82ba-43b2-a5cc-1ccfb4b6a734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62ca34e6-473d-427f-aaf3-15364eb9d79b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84367d01-f42d-4865-9ad5-ad1068857683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62ca34e6-473d-427f-aaf3-15364eb9d79b",
                    "LayerId": "7421c57d-3c03-4b92-9227-e6063ce55320"
                }
            ]
        },
        {
            "id": "1d3a3c89-c17e-42b5-8f56-84d169633411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde1c97f-1284-41c0-b560-bac02ea5f4c6",
            "compositeImage": {
                "id": "90f522ac-5da6-4cad-9801-616277eafbec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d3a3c89-c17e-42b5-8f56-84d169633411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "826c90a4-5892-428a-8f48-4989e3ee1a1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d3a3c89-c17e-42b5-8f56-84d169633411",
                    "LayerId": "7421c57d-3c03-4b92-9227-e6063ce55320"
                }
            ]
        },
        {
            "id": "0295a661-72e3-4e80-8a8b-d782e59253fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde1c97f-1284-41c0-b560-bac02ea5f4c6",
            "compositeImage": {
                "id": "97054289-d49d-46b8-a0e6-c271065bd449",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0295a661-72e3-4e80-8a8b-d782e59253fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb6cfe82-02c1-48f9-94f5-709452c20da9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0295a661-72e3-4e80-8a8b-d782e59253fc",
                    "LayerId": "7421c57d-3c03-4b92-9227-e6063ce55320"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "7421c57d-3c03-4b92-9227-e6063ce55320",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dde1c97f-1284-41c0-b560-bac02ea5f4c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}