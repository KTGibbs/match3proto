{
    "id": "cc944bf0-123c-431d-9735-1ff0a13cf25f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1279,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cb7c242-783d-44fd-ab02-9ea4d527eaf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc944bf0-123c-431d-9735-1ff0a13cf25f",
            "compositeImage": {
                "id": "be5474da-9392-430b-91ed-cf36c4522eee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cb7c242-783d-44fd-ab02-9ea4d527eaf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb7eeda-b410-456d-bbe7-216aff2e6eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cb7c242-783d-44fd-ab02-9ea4d527eaf2",
                    "LayerId": "8c704ea6-fd69-4d8a-8f7e-50c1b3f99258"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "8c704ea6-fd69-4d8a-8f7e-50c1b3f99258",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc944bf0-123c-431d-9735-1ff0a13cf25f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}