{
    "id": "0cecb0f9-58e1-448e-a4fb-8f5e8bc19f87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f8b406f-3377-4516-9f73-64d434d4cfb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0cecb0f9-58e1-448e-a4fb-8f5e8bc19f87",
            "compositeImage": {
                "id": "f4610ad6-4de9-45b0-9512-30b32e2f9d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f8b406f-3377-4516-9f73-64d434d4cfb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fa83abe-c8f5-44a0-828c-360614304082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f8b406f-3377-4516-9f73-64d434d4cfb8",
                    "LayerId": "ad1e1a79-26ae-49cd-8102-35da79a152b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "ad1e1a79-26ae-49cd-8102-35da79a152b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0cecb0f9-58e1-448e-a4fb-8f5e8bc19f87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}