{
    "id": "d6157197-cfb7-4a2e-9f66-ee539759f840",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_selectedBox",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f6ff87e-b4c0-47a0-b824-6182df07f201",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6157197-cfb7-4a2e-9f66-ee539759f840",
            "compositeImage": {
                "id": "77fa26fd-03e1-4708-b506-c1cc3b1fb209",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f6ff87e-b4c0-47a0-b824-6182df07f201",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc222ed6-9e84-4d8f-97e8-098fe97f14ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f6ff87e-b4c0-47a0-b824-6182df07f201",
                    "LayerId": "c488250e-c4ee-49b3-ba51-31f845b45cf5"
                }
            ]
        },
        {
            "id": "3bf75318-519e-4231-971c-5cf1c65d60ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6157197-cfb7-4a2e-9f66-ee539759f840",
            "compositeImage": {
                "id": "85e3adc6-a227-4321-ae38-8eb2b61feaee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bf75318-519e-4231-971c-5cf1c65d60ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21e058dc-a454-4576-9fb7-a0ce79842d22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bf75318-519e-4231-971c-5cf1c65d60ff",
                    "LayerId": "c488250e-c4ee-49b3-ba51-31f845b45cf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c488250e-c4ee-49b3-ba51-31f845b45cf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6157197-cfb7-4a2e-9f66-ee539759f840",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}