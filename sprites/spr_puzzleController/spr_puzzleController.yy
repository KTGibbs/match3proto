{
    "id": "af7822ee-0e6e-41a7-8a17-2578f5f0d494",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_puzzleController",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5c22998-606d-46ff-830f-5cfb0d996454",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af7822ee-0e6e-41a7-8a17-2578f5f0d494",
            "compositeImage": {
                "id": "b9d6593b-ec86-48e5-97c3-78424dd5c4f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c22998-606d-46ff-830f-5cfb0d996454",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23617877-e885-4398-9d7b-288b372506bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c22998-606d-46ff-830f-5cfb0d996454",
                    "LayerId": "ac7a8030-43ea-4bc8-b055-38477514abd9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ac7a8030-43ea-4bc8-b055-38477514abd9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af7822ee-0e6e-41a7-8a17-2578f5f0d494",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}